package org.eclipse.osbp.jpa.services;

import java.util.Map;

import org.eclipse.osbp.runtime.common.filter.IJPQL;

@SuppressWarnings("serial")
public class JPQL implements IJPQL {

	private final String jpql;
	private final Map<String, Object> params;

	public static JPQL create(String jpql, Map<String, Object> params) {
		return new JPQL(jpql, params);
	}

	public JPQL(String jpql, Map<String, Object> params) {
		super();
		this.jpql = jpql;
		this.params = params;
	}

	@Override
	public String getJPQL() {
		return jpql;
	}

	@Override
	public Map<String, Object> getParameters() {
		return params;
	}

}
