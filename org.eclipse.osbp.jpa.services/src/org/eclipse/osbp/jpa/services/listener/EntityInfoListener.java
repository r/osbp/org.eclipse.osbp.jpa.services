package org.eclipse.osbp.jpa.services.listener;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.eclipse.osbp.runtime.common.session.ISession;
import org.eclipse.osbp.runtime.common.util.BeanUtils;

public class EntityInfoListener {

	@PrePersist
	public void onCreate(Object bean){
		ISession session = ISession.getCurrent();
		if(session != null){
			String user = (String) session.get("userId");
			BeanUtils.setCreateUser(bean, user);
			BeanUtils.setCreateAt(bean, new Date());
		}
	}
	
	@PreUpdate
	public void onUpdate(Object bean){
		ISession session = ISession.getCurrent();
		if(session != null){
			String user = (String) session.get("userId");
			BeanUtils.setUpdateUser(bean, user);
			BeanUtils.setUpdateAt(bean, new Date());
			if(BeanUtils.isHistorized(bean.getClass())) {
				System.out.println("hist");
			}
		}
	}
}
