package org.eclipse.osbp.jpa.services.history;

import org.eclipse.persistence.config.DescriptorCustomizer;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.exceptions.OptimisticLockException;
import org.eclipse.persistence.history.HistoryPolicy;
import org.eclipse.persistence.queries.ModifyQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HistoryCustomizer implements DescriptorCustomizer {

	static final Logger LOGGER = LoggerFactory.getLogger(HistoryCustomizer.class);
	
	@SuppressWarnings("serial")
	public void customize(ClassDescriptor descriptor) {
		HistoryPolicy policy = new HistoryPolicy(){
			@Override
			public void logicalDelete(ModifyQuery writeQuery, boolean isUpdate, boolean isShallow) {
				try {
					super.logicalDelete(writeQuery, isUpdate, isShallow);
				} catch (OptimisticLockException e) {
					LOGGER.warn("Workaround for bug. If postUpdate is called, but history does not exist, this exception occurs. {}", e);
				}
			}
		};
		policy.addStartFieldName("HIST__START_DATE");
		policy.addEndFieldName("HIST__END_DATE");
		policy.addHistoryTableName(descriptor.getTableName(), descriptor.getTableName() + "_HIST");
		descriptor.setHistoryPolicy(policy);
	}
}
