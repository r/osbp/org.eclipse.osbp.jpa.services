package org.eclipse.osbp.jpa.services.history;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Version;

import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.eclipse.osbp.runtime.common.annotations.HistDomainKey;
import org.eclipse.osbp.runtime.common.annotations.HistIsCurrent;
import org.eclipse.osbp.runtime.common.annotations.HistIsCustomVersion;
import org.eclipse.osbp.runtime.common.annotations.HistValidUntil;
import org.eclipse.osbp.runtime.common.historized.UUIDHist;
import org.eclipse.osbp.runtime.common.util.BeanUtils;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.mappings.DatabaseMapping;

public class HistorizedDescriptorWrapper {

	private final Class<?> clazz;
	private final ClassDescriptor descriptor;
	private final SimpleDateFormat dateFormat;
	private DatabaseMapping versionMapping;
	private DatabaseMapping validUntilMapping;
	private DatabaseMapping currentMapping;
	private DatabaseMapping isCustomVersionMapping;

	private DatabaseMapping historizedDomainKeyMapping;
	private DatabaseMapping domainKeyMapping;

	private DatabaseMapping idMapping;
	private ClassDescriptor uuidHistDescriptor;
	private DatabaseMapping id_idMapping;
	private DatabaseMapping id_validFromMapping;

	public HistorizedDescriptorWrapper(ClassDescriptor descriptor) {
		this.clazz = descriptor.getJavaClass();
		this.descriptor = descriptor;

		// later may be controlled by the descriptor
		dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		// find the ID String field by the descriptor
		List<DatabaseField> pk = descriptor.getPrimaryKeyFields();
		for (DatabaseMapping mapping : descriptor.getMappings()) {
			if (mapping.isPrimaryKeyMapping()) {
				idMapping = mapping;
				break;
			}
		}

		uuidHistDescriptor = idMapping.getReferenceDescriptor();

		Field versionField = BeanUtils.getField(clazz, Version.class);
		Field validUntilField = BeanUtils.getField(clazz, HistValidUntil.class);
		Field currentHist = BeanUtils.getField(clazz, HistIsCurrent.class);
		Field isCustomVersion = BeanUtils.getField(clazz, HistIsCustomVersion.class);
		Field historizedDomainKeyField = BeanUtils.getField(clazz, HistDomainKey.class);
		Field domainKeyField = BeanUtils.getField(clazz, DomainKey.class);

		id_idMapping = uuidHistDescriptor.getMappingForAttributeName("id");
		id_validFromMapping = uuidHistDescriptor.getMappingForAttributeName("validFrom");
		versionMapping = descriptor.getMappingForAttributeName(versionField.getName());
		validUntilMapping = descriptor.getMappingForAttributeName(validUntilField.getName());
		currentMapping = descriptor.getMappingForAttributeName(currentHist.getName());
		isCustomVersionMapping = descriptor.getMappingForAttributeName(isCustomVersion.getName());
		versionMapping = descriptor.getMappingForAttributeName(versionField.getName());
		historizedDomainKeyMapping = descriptor.getMappingForAttributeName(historizedDomainKeyField.getName());
		domainKeyMapping = descriptor.getMappingForAttributeName(domainKeyField.getName());
	}

	public String getIdAttName() {
		return idMapping.getAttributeName();
	}

	public String getId_IdAttName() {
		return id_idMapping.getAttributeName();
	}

	public String getId_ValidFromAttName() {
		return id_validFromMapping.getAttributeName();
	}

	public String getVersionAttName() {
		return versionMapping.getAttributeName();
	}

	public String getValidUntilAttName() {
		return validUntilMapping.getAttributeName();
	}

	public String getCurrentHistAttName() {
		return currentMapping.getAttributeName();
	}

	public String getIsCustomVersionAttName() {
		return isCustomVersionMapping.getAttributeName();
	}

	public String getHistorizedDomainKeyAttName() {
		return historizedDomainKeyMapping.getAttributeName();
	}

	public UUIDHist getID(Object obj) {
		return (UUIDHist) idMapping.getAttributeValueFromObject(obj);
	}

	public int getVersion(Object obj) {
		return (int) versionMapping.getAttributeValueFromObject(obj);
	}

	public long getId_ValidFrom(Object obj) {
		if (obj instanceof UUIDHist) {
			return ((UUIDHist) obj).getValidFrom();
		}
		UUIDHist idHist = (UUIDHist) idMapping.getAttributeValueFromObject(obj);
		return idHist != null ? idHist.getValidFrom() : -1;
	}

	public String getId_IdFrom(Object obj) {
		if (obj instanceof UUIDHist) {
			return ((UUIDHist) obj).getId();
		}
		UUIDHist idHist = (UUIDHist) idMapping.getAttributeValueFromObject(obj);
		return idHist != null ? idHist.getId() : null;
	}

	public long getValidUntil(Object obj) {
		return (long) validUntilMapping.getAttributeValueFromObject(obj);
	}

	public boolean isCurrentHist(Object obj) {
		return (boolean) currentMapping.getAttributeValueFromObject(obj);
	}

	public boolean isCustomVersion(Object obj) {
		return (boolean) isCustomVersionMapping.getAttributeValueFromObject(obj);
	}

	public String getHistorizedDomainKey(Object obj) {
		return (String) historizedDomainKeyMapping.getAttributeValueFromObject(obj);
	}

	public Class<?> getEntityClass() {
		return descriptor.getJavaClass();
	}

	public void setID(Object obj, Object value) {
		idMapping.setAttributeValueInObject(obj, value);
	}

	public void setVersion(Object obj, int value) {
		versionMapping.setAttributeValueInObject(obj, value);
	}

	public void setId_ValidFrom(Object obj, long value) {
		if (obj instanceof UUIDHist) {
			((UUIDHist) obj).setValidFrom(value);
		}
		UUIDHist idHist = (UUIDHist) idMapping.getAttributeValueFromObject(obj);
		if (idHist != null) {
			idHist.setValidFrom(value);
		}
	}

	public void setId_IdFrom(Object obj, String value) {
		if (obj instanceof UUIDHist) {
			((UUIDHist) obj).setId(value);
		}
		UUIDHist idHist = (UUIDHist) idMapping.getAttributeValueFromObject(obj);
		if (idHist != null) {
			idHist.setId(value);
		}
	}

	public void setValidUntil(Object obj, long value) {
		validUntilMapping.setAttributeValueInObject(obj, value);
	}

	public void setCurrentHist(Object obj, boolean value) {
		currentMapping.setAttributeValueInObject(obj, value);
	}

	public void setCustomVersion(Object obj, boolean value) {
		isCustomVersionMapping.setAttributeValueInObject(obj, value);
	}

	public void setHistorizedDomainKey(Object obj, String value) {
		historizedDomainKeyMapping.setAttributeValueInObject(obj, value);
	}

	public void applyHistorizedDomainKey(Object obj) {
		if (domainKeyMapping == null) {
			return;
		}

		String domainKey = (String) domainKeyMapping.getAttributeValueFromObject(obj);
		Date date = new Date(getId_ValidFrom(obj));

		String histDomainKey = domainKey + "-" + dateFormat.format(date);
		setHistorizedDomainKey(obj, histDomainKey);

	}

}
