package org.eclipse.osbp.jpa.services.history;

import org.eclipse.osbp.runtime.common.historized.UUIDHist;
import org.eclipse.persistence.descriptors.ClassDescriptor;

/**
 * This class gives access to historized annotated fields in the given obj.
 */
public class HistorizedObjectWrapper {

	private HistorizedDescriptorWrapper delegate;
	private Object obj;

	public HistorizedObjectWrapper(ClassDescriptor descriptor, Object obj) {
		delegate = new HistorizedDescriptorWrapper(descriptor);
		this.obj = obj;
	}

	public String getIdAttName() {
		return delegate.getIdAttName();
	}

	public String getVersionAttName() {
		return delegate.getVersionAttName();
	}

	public String getId_IdAttName() {
		return delegate.getId_IdAttName();
	}

	public String getId_ValidFromAttName() {
		return delegate.getId_ValidFromAttName();
	}

	public long getId_ValidFrom() {
		return delegate.getId_ValidFrom(obj);
	}

	public String getId_IdFrom() {
		return delegate.getId_IdFrom(obj);
	}

	public void setId_ValidFrom(long value) {
		delegate.setId_ValidFrom(obj, value);
	}

	public void setId_IdFrom(String value) {
		delegate.setId_IdFrom(obj, value);
	}

	public String getValidUntilAttName() {
		return delegate.getValidUntilAttName();
	}

	public String getCurrentHistAttName() {
		return delegate.getCurrentHistAttName();
	}

	public String getIsCustomVersionAttName() {
		return delegate.getIsCustomVersionAttName();
	}

	public UUIDHist getID() {
		return delegate.getID(obj);
	}

	public long getVersion() {
		return delegate.getVersion(obj);
	}

	public long getValidUntil() {
		return delegate.getValidUntil(obj);
	}

	public boolean isCurrentHist() {
		return delegate.isCurrentHist(obj);
	}

	public boolean isCustomVersion() {
		return delegate.isCustomVersion(obj);
	}

	public Class<?> getEntityClass() {
		return delegate.getEntityClass();
	}

	public void setID(Object value) {
		delegate.setID(obj, value);
	}

	public void setVersion(int value) {
		delegate.setVersion(obj, value);
	}

	public void setValidUntil(long value) {
		delegate.setValidUntil(obj, value);
	}

	public void setCurrentHist(boolean value) {
		delegate.setCurrentHist(obj, value);
	}

	public void setCustomVersion(boolean value) {
		delegate.setCustomVersion(obj, value);
	}

	public String getHistorizedDomainKeyAttName() {
		return delegate.getHistorizedDomainKeyAttName();
	}

	public String getHistorizedDomainKey() {
		return delegate.getHistorizedDomainKey(obj);
	}

	public void setHistorizedDomainKey(String value) {
		delegate.setHistorizedDomainKey(obj, value);
	}

	public void applyHistorizedDomainKey() {
		delegate.applyHistorizedDomainKey(obj);
	}

}
