/*
 * Copyright 2000-2014 Vaadin Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * Contributor:
 * 		Florian Pirchner - Copied filter and fixed them for osbp usecase.
 */
package org.eclipse.osbp.jpa.services.filters;

import java.util.List;

import org.eclipse.osbp.runtime.common.filter.ILFilter;

public class LIn implements ILFilter {

    private final String propertyId;
    private final List<Comparable<?>> values;

    public LIn(String propertyId, List<Comparable<?>> values) {
        this.propertyId = propertyId;
        this.values = values;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public List<Comparable<?>> getValues() {
        return values;
    }

    @Override
    public boolean appliesToProperty(Object propertyId) {
        return getPropertyId() != null && getPropertyId().equals(propertyId);
    }

    @Override
    public int hashCode() {
    	int valuesHashCode = 0;
    	for (Comparable<?> value : getValues()) {
			valuesHashCode = valuesHashCode + value.hashCode();
		}
        return getPropertyId().hashCode() + valuesHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        // Only objects of the same class can be equal
        if (!getClass().equals(obj.getClass())) {
            return false;
        }
        final LIn o = (LIn) obj;

        // Checks the properties one by one
        boolean propertyIdEqual = (null != getPropertyId()) ? getPropertyId()
                .equals(o.getPropertyId()) : null == o.getPropertyId();
//        boolean startValueEqual = (null != getStartValue()) ? getStartValue()
//                .equals(o.getStartValue()) : null == o.getStartValue();
//        boolean endValueEqual = (null != getEndValue()) ? getEndValue().equals(
//                o.getEndValue()) : null == o.getEndValue();
//        return propertyIdEqual && startValueEqual && endValueEqual;
        return propertyIdEqual;

    }
}
