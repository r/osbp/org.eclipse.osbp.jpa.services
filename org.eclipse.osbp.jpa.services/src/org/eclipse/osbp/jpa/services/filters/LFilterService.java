package org.eclipse.osbp.jpa.services.filters;

import org.eclipse.osbp.jpa.services.Query;
import org.eclipse.osbp.jpa.services.filters.LCompare.Equal;
import org.eclipse.osbp.runtime.common.filter.ILFilter;
import org.eclipse.osbp.runtime.common.filter.ILFilterService;
import org.eclipse.osbp.runtime.common.filter.IQuery;
import org.osgi.service.component.annotations.Component;

@Component
public class LFilterService implements ILFilterService {

	@Override
	public ILFilter createUniqueEntryFilter(Object value, String valueProperty, Object id, String idProperty) {
		// where {idProperty} <> {id} and {valueProperty} = {value}

		LNot idNotEqual = new LNot(new LCompare.Equal(idProperty, id));
		Equal valueEqual = new LCompare.Equal(valueProperty, value);

		return new LAnd(idNotEqual, valueEqual);
	}

	@Override
	public IQuery createQuery(ILFilter filter) {
		return new Query(filter);
	}

}
