/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.jpa.services;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.osbp.runtime.common.filter.ILFilter;
import org.eclipse.osbp.runtime.common.filter.IQuery;
import org.eclipse.osbp.runtime.common.filter.SortOrder;

@SuppressWarnings("serial")
public class Query implements IQuery {

	private ILFilter filter;
	private final SortOrder sortOrder;
	private final Map<String, Object> map;
	public static final String ENTITY_CLASSES_KEY = "entityClassesKey";
	public static final String DATAMART_CLASSES_KEY = "datamartClassesKey";

	public Query() {
		this((ILFilter) null);
	}

	public Query(ILFilter filter) {
		this(filter, null);
	}

	public Query(ILFilter filter, SortOrder sortOrder) {
		this(filter, sortOrder, new HashMap<String, Object>());
	}

	public Query(ILFilter filter, SortOrder sortOrder, Map<String, Object> map) {
		super();
		this.filter = filter;
		this.sortOrder = sortOrder != null ? sortOrder : new SortOrder();
		this.map = map;
	}

	@Override
	public ILFilter getFilter() {
		return filter;
	}

	@Override
	public SortOrder getSortOrder() {
		return sortOrder;
	}

	@Override
	public void replaceFilter(ILFilter filter) {
		this.filter = filter;
	}

	@Override
	public Map<String, Object> getMap() {
		return map;
	}

}
